/**********************************************************************************
** Authors: Emily Falkner, Jeff Tribble
** Class: CSE 430: Operating Systems
** Professor: Partha Dasgupta
** Assignment: Project 4
** File Name: proj-4.c
***********************************************************************************/
// HEADER FILES
#include "sem.h"

// GLOBAL DEFINITIONS
#define NUM_READERS 4 // how many readers do we want?
#define NUM_WRITERS 2 // how many writers do we want?

// GLOBAL VARIABLES
TCB_t *threads[NUM_READERS + NUM_WRITERS]; // reader & writer threads
char *documentContents; // current document message
char *messages[6]; // different messages to read & write
int currentMessage; // the current message
struct sem *wsem; // writers semaphore
struct sem *rsem; // readers semaphore
struct sem *mutex; // enforce mutual exclusion
int wwc; // writer wait count
int wc; // writer count
int rwc; // reader wait count
int rc; // reader count

// FOWARD DECLARATIONS
void writer();
void writerEntry();
void writerExit();
void reader();
void readerEntry();
void readerExit();

// FUNCTION DEFINITIONS

/**
 * Writer
 */
void writer()
{
  while (1) {
    writerEntry();

    P(mutex);
    documentContents = messages[++currentMessage % 6];
    printf("Writing #%p is writing: %s\n", runQ->head, documentContents);
    sleep(1);
    V(mutex);

    writerExit();
  }
}

/**
 * Writer entry
 */
void writerEntry()
{
  P(mutex);

  if (rc > 0 || wc > 0) {
    wwc++;
    V(mutex);
    P(wsem);
    wwc--;
  }

  wc++;

  V(mutex);
}

/**
 * Writer exit
 */
void writerExit()
{
  P(mutex);

  wc--;

  if (rwc > 0) {
    V(rsem);
  } else if (wwc > 0) {
    V(wsem);
  } else {
    V(mutex);
  }
}

/**
 * Reader
 */
void reader()
{
  while (1) {
    readerEntry();

    P(mutex);
    printf("Reader #%p is reading: %s\n", runQ->head, documentContents);
    sleep(1);
    V(mutex);

    readerExit();
  }
}

/**
 * Reader entry
 */
void readerEntry()
{
  P(mutex); // acquire the critical section

  if (wwc > 0 || wc > 0) {
    rwc++;
    V(mutex);
    P(rsem);
    rwc--;
  }

  rc++; // become a reader

  // if there are readers waiting...
  if (rwc > 0) {
    V(rsem); // ...wake up a reader
  }
  // otherwise, since you're the last one waiting...
  else {
    V(mutex); // ...release the critical section
  }
}

/**
 * Reader exit
 */
void readerExit()
{
  P(mutex); // acquire the critical section

  rc--; // quit being a reader

  // if you're the last reader and there are writers waiting...
  if (rc == 0 && wwc > 0) {
    V(wsem); // ...let a writer in
  } else {
    V(mutex); // release the critical section
  }
}

int main(char** args)
{
  // initialize semaphores
  wsem = (struct sem*) malloc(sizeof(struct sem));
  rsem = (struct sem*) malloc(sizeof(struct sem));
  mutex = (struct sem*) malloc(sizeof(struct sem));
  initSem(wsem, 0);
  initSem(rsem, 0);
  initSem(mutex, 1);

  // initialize run queue
  runQ = (struct que*) malloc(sizeof(struct que));
  initQueue(runQ);

  // initialize messages
  messages[0] = "Dasgupta is the best!";
  messages[1] = "The tests are pretty hard though...";
  messages[2] = "...but the projects aren't too bad...";
  messages[3] = "...unless you get a segfault.";
  messages[4] = "Why can't we just have all the memory?";
  messages[5] = "SEGFAULTSEGFAULTSEGFAULTSEGFAULTSEGFAULT";
  currentMessage = 0;
  documentContents = messages[0];

  // enter readers and writers at random intervals
  int i, rSpots, wSpots;
  rSpots = NUM_READERS;
  wSpots = NUM_WRITERS;
  while (rSpots > 0 || wSpots > 0) {
    if (rand() % 2 == 0 && rSpots > 0) {
      startThread(threads[i], reader); rSpots--;
    } else if (wSpots > 0) {
      startThread(threads[i], writer); wSpots--;
    }
  }

  run();

  return 0;
}
