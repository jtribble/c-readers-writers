/******************************************************************************************************
** Authors: Emily Falkner, Jeff Tribble
** Class: CSE 430: Operating Systems
** Professor: Partha Dasgupta
** Assignment: Project 4
** File Name: threads.h
******************************************************************************************************/
// HEADER GUARD
#ifndef THREADS_H
#define THREADS_H

// HEADER FILES
#include "q.h"

// GLOBAL VARIABLES
struct que *runQ;

// FUNCTION DEFINITIONS
void startThread(TCB_t *tcb, void (*function)(void))
{
  tcb = (TCB_t *) malloc(sizeof(TCB_t));
  void *stackOfStacks = (void *) malloc(8192);
  initTcb(tcb, function, stackOfStacks, 8192);
  addQueue(runQ, tcb);
}

void run()
{
  ucontext_t parent;
  getcontext(&parent);
  swapcontext(&parent, &(runQ->head->context));
}

void yeild()
{
  TCB_t *oldHead = runQ->head;
  rotateQueue(runQ);
  swapcontext(&(oldHead->context), &(runQ->head->context));
}

// END HEADER GUARD
#endif
