/******************************************************************************************************
** Authors: Emily Falkner, Jeff Tribble
** Class: CSE 430: Operating Systems
** Professor: Partha Dasgupta
** Assignment: Project 4
** File Name: sem.h
******************************************************************************************************/
// HEADER GUARDS
#ifndef SEM_H
#define SEM_H

// HEADER FILES
#include "threads.h"

// DATA STRUCTURES
typedef struct sem {
  int val; // stores the sphore value
  struct que *sleepQ; // stores the queue for the blocked processes
} sem;

// FORWARD DECLARATIONS
void createSem(int);
void initSem(sem*, int);
void P(sem*);
void V(sem*);

// FUNCTION DEFINITIONS

/**
 * Factory method for initializing a semaphore
 *
 * @param {int} initialValue
 */
void initSem(sem* s, int value)
{
  s->sleepQ = (struct que*) malloc(sizeof(struct que));
  initQueue(s->sleepQ);
  s->val = value;
}

void P(sem *s)
{
  struct TCB_t *tcb;

  s->val--;

  if (s->val < 0) {
    tcb = deleteQueue(runQ);
    addQueue(s->sleepQ, tcb);
    swapcontext(&(tcb->context), &(runQ->head->context));
  }
}

void V(sem *s)
{
  struct TCB_t *tcb;

  s->val++;

  if (s->val <= 0) {
    tcb = deleteQueue(s->sleepQ);
    addQueue(runQ, tcb);
  }

  yeild();
}

// END HEADER GUARD
#endif
