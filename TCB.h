/******************************************************************************************************
** Authors: Emily Falkner, Jeff Tribble
** Class: CSE 430: Operating Systems
** Professor: Partha Dasgupta
** Assignment: Project 4
** File Name: TCB.h
******************************************************************************************************/
// START HEADER GUARD
#ifndef TCB_H
#define TCB_H

// HEADER FILES
#include <ucontext.h>
#include <stdio.h>
#include <string.h>

// DATA STRUCTURES
typedef struct TCB_t {
  struct TCB_t *next;
  struct TCB_t *prev;
  ucontext_t context;
} TCB_t;

// FUNCTION DEFINITIONS
void initTcb(TCB_t *tcb, void *function, void *stackP, int stackSize)
{
  memset(tcb, '\0', sizeof(TCB_t));
  getcontext(&tcb->context);
  tcb->context.uc_stack.ss_sp = stackP;
  tcb->context.uc_stack.ss_size = (size_t) stackSize;
  makecontext(&tcb->context, function, 0);
}

// END HEADER GUARD
#endif
