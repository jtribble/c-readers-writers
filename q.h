/******************************************************************************************************
** Authors: Emily Falkner, Jeff Tribble
** Class: CSE 430: Operating Systems
** Professor: Partha Dasgupta
** Assignment: Project 4
** File Name: q.h
******************************************************************************************************/
// START HEADER GUARD
#ifndef Q_H
#define Q_H

// HEADER FILES
#include <stdlib.h>
#include <stdio.h>
#include "TCB.h"

// DATA STRUCTURES
typedef struct que {
  struct TCB_t *head; // pointer to first element
} que;

// FUNCTION DEFINITIONS

/**
 * Initialize a new queue
 *
 * @return {struct que*}
 */
void initQueue(que *q)
{
  q->head = NULL;
}

void addQueue(que *q, TCB_t *tcb)
{
  // if the queue is empty, tcb is new head
  if (q->head == NULL)
  {
    q->head = tcb;
    tcb->prev = NULL;
    tcb->next = NULL;
  }
  // else, if there are at least two elements in the queue...
  else if (q->head->next != NULL)
  {
    tcb->prev = q->head->prev; // link new head (tcb) with last element
    q->head->prev->next = tcb; // link last element to new head (tcb)
    tcb->next = q->head; // shift tcb onto head
    q->head->prev = tcb; // update old head's prev
  }
  // otherwise, queue has one element
  else
  {
    q->head->next = tcb; // add new link to the item
    q->head->prev = tcb; // link to the last element
    tcb->next = q->head; // link to first element
    tcb->prev = q->head; // link to the old item
  }
}

struct TCB_t* deleteQueue(que *q)
{
  struct TCB_t *tcb = q->head;

  // if the queue is non-empty
  if (q->head != NULL) {
    q->head->prev->next = q->head->next;
    q->head->next->prev = q->head->prev;
    q->head = q->head->next;
  }

  return tcb;
}

void rotateQueue(que *q)
{
  addQueue(q, deleteQueue(q));
}

// END HEADER GUARD
#endif
